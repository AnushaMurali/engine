syntax = "proto3";
package cdm;
option java_package = "com.kitware.physiology.cdm";
option optimize_for = SPEED;

import "cdm/Properties.proto";

/** @brief Data used to configure the patient.  */
message PatientData
{
  /** @brief Enumeration for patient sex */
  enum eSex
  {
    Male=0;   /**<< @brief Male patient. */
    Female=1; /**<< @brief Female patient. */
  }
  
  /** Physical Characteristics */
  string                   Name                                  = 1; /**<< @brief Patient Identifier. */
  eSex                     Sex                                   = 2; /**<< @brief @ref PatientData_eSexTable */
  ScalarTimeData           Age                                   = 3; /**<< @brief Patient age. */
  ScalarMassData           Weight                                = 4; /**<< @brief Patient weight. */
  ScalarLengthData         Height                                = 5; /**<< @brief Patient height. */
  ScalarMassPerVolumeData  BodyDensity                           = 6; /**<< @brief  The mass of the body divided by the volume of the body. */
  Scalar0To1Data           BodyFatFraction                       = 7; /**<< @brief Total mass of fat divided by total body mass; body fat includes essential body fat and storage body fat. */
  ScalarMassData           LeanBodyMass                          = 8; /**<< @brief Component of body composition after subtracting body fat weight. */
  /** Physical Characteristics that can change and should be change due to a condition */
  ScalarAreaData           AlveoliSurfaceArea                    = 9; /**<< @brief The surface area of the alveoli for gas diffusion */
  Scalar0To1Data           RightLungRatio                        = 10;/**<< @brief The ratio of total lung surface area that is associated with the right lung. */
  ScalarAreaData           SkinSurfaceArea                       = 11;/**<< @brief The surface area of the skin surface of the body. */
  /** Systemic Targets to tune the engine to */
  ScalarPowerData          BasalMetabolicRate                    = 12;/**<< @brief The life-sustaining metabolic rate. */
  ScalarVolumeData         BloodVolumeBaseline                   = 13;/**<< @brief The total volume of fluid within the cardiovascular system. */
  ScalarPressureData       DiastolicArterialPressureBaseline     = 14;/**<< @brief The starting minimum pressure in the aorta over the course of a cardiac cycle. */
  ScalarFrequencyData      HeartRateBaseline                     = 15;/**<< @brief The resting heart rate. */
  ScalarPressureData       MeanArterialPressureBaseline          = 16;/**<< @brief The resting mean arterial pressure. */
  ScalarFrequencyData      RespirationRateBaseline               = 17;/**<< @brief The resting respiration rate. */
  ScalarPressureData       SystolicArterialPressureBaseline      = 18;/**<< @brief The starting maximum pressure in the aorta over the course of a cardiac cycle. */
  ScalarVolumeData         TidalVolumeBaseline                   = 19;/**<< @brief The starting volume of air moved into or out of the lungs during normal respiration. */
  /** Cardiovascular Systemic Bounds, generally computed based on physical characteristics */
  ScalarFrequencyData      HeartRateMaximum                      = 20;/**<< @brief The maximum heart rate the patient can withstand. */
  ScalarFrequencyData      HeartRateMinimum                      = 21;/**<< @brief The minimum heart rate the patient can withstand. */
  /** Respiratory Systemic Bounds, generally computed based on physical characteristics */
  ScalarVolumeData         ExpiratoryReserveVolume               = 22;/**<< @brief The maximum amount of air that can be exhaled from the end-expiratory position. */
  ScalarVolumeData         FunctionalResidualCapacity            = 23;/**<< @brief The volume of air remaining in the lungs after a passive expiration. */
  ScalarVolumeData         InspiratoryCapacity                   = 24;/**<< @brief The sum of IRV and TV. */
  ScalarVolumeData         InspiratoryReserveVolume              = 25;/**<< @brief The maximal volume that can be inhaled from the end-inspiratory level. */
  ScalarVolumeData         ResidualVolume                        = 26;/**<< @brief The volume of air remaining in the lungs after a maximal exhalation. */
  ScalarVolumeData         TotalLungCapacity                     = 27;/**<< @brief The volume of air in the lungs at maximal inflation. */
  ScalarVolumeData         VitalCapacity                         = 28;/**<< @brief The maximum amount of air a person can expel from their lungs after a maximum inhalation. */
}
