/* Distributed under the Apache License, Version 2.0.
   See accompanying NOTICE file for details.*/
package com.kitware.physiology.utilities.csv.plots;

import com.kitware.physiology.datamodel.properties.SEScalarTime;

public class LogEvent
{
  protected SEScalarTime time = new SEScalarTime();
  protected String text;
}
